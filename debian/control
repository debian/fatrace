Source: fatrace
Section: utils
Priority: optional
Build-Depends: debhelper-compat (= 13)
Maintainer: Martin Pitt <mpitt@debian.org>
Homepage: https://github.com/martinpitt/fatrace
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/debian/fatrace.git
Vcs-Browser: https://salsa.debian.org/debian/fatrace

Package: fatrace
Architecture: linux-any
Depends: ${shlibs:Depends},
 ${misc:Depends}
Recommends: python3, powertop
Description: report system wide file access events
 fatrace reports file access events from all running processes.
 Its  main  purpose  is to find processes which keep waking up the disk
 unnecessarily and thus prevent some power saving.
 .
 This package also contains a "power-usage-report" tool, which uses
 fatrace and powertop to build a textual report from one minute of
 measuring power usage and file accesses. This does not take any
 arguments or requires any interactivity, so is very simple to use and
 serves as a starting point for bug reports or optimizing a particular
 installation.
